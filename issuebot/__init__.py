import binascii
import configparser
import git
import gitlab
import glob
import hashlib
import json
import os
import re
import requests
import subprocess
import sys

from colorama import Fore, Style
from datetime import datetime
from fdroidserver import common
from fdroidserver import net
from urllib.parse import urlparse

HEADERS = {'User-Agent': 'F-Droid Issuebot'}
REQUESTS_TIMEOUT = 120

ISSUEBOT_API_DIR = 'public/issuebot/'  # trailing slash makes it work with .startswith()

IZZYSOFT_PATTERN = re.compile(r'''.*<a [^>]* href=['"](.+)['"][^>]*>Download.*''')
JAVA_PACKAGENAME = (
    r'''(?:[a-zA-Z]+(?:\d*[a-zA-Z_]*)*)(?:\.[a-zA-Z]+(?:\d*[a-zA-Z_]*)*)+'''
)
GIT_PATTERN = re.compile(
    r'http[s]?://(github.com|gitlab.com|bitbucket.org|git.code.sf.net|codeberg.org|framagit.org)/[\w.-]+/[\w.-]+'
)
GPLAY_PATTERN = re.compile(
    r'http[s]?://play\.google\.com/store/apps/details\?id=%s' % JAVA_PACKAGENAME
)
APPLICATION_ID_PATTERN = re.compile(
    r'''(?:APPLICATION|app|PACKAGE) *(?:ID|NAME|)[:=\s]+['"]?(%s)''' % JAVA_PACKAGENAME,
    re.IGNORECASE | re.DOTALL,
)
APK_DOWNLOAD_URL_PATTERN = re.compile(
    r'https?://[^ /]+/[^ :]+\.apk', flags=re.IGNORECASE
)
# BCP47 language tags used by Fastlane/Triple-T
BCP47_LOCALE_TAG_PATTERN = re.compile(r"[a-z]{2,3}(-([A-Z][a-zA-Z]+|\d+|[a-z]+))*")


class Encoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, set):
            return sorted(obj)
        return super().default(obj)


class IssuebotModule:
    reply = {'emoji': set(), 'labels': set(), 'report': '', 'reportData': dict()}

    def __init__(self):
        self.application_id = os.getenv('ISSUEBOT_CURRENT_APPLICATION_ID', '').strip()
        if self.application_id:
            self.reply['applicationId'] = self.application_id
        job_id, issue_id = get_job_issue_ids()
        self.job_id = int(job_id)
        self.issue_id = int(issue_id)
        cwd = os.getcwd()
        self.base_dir = cwd
        self.apkfiles = []
        self.apkfiles += glob.glob(
            os.path.join(cwd, 'unsigned', self.application_id + '_*.apk')
        )
        self.apkfiles += glob.glob(
            os.path.join(cwd, 'repo', self.application_id + '_*.apk')
        )
        self.source_url = os.getenv('ISSUEBOT_CURRENT_SOURCE_URL', '').strip()
        if self.source_url:
            self.reply['sourceUrl'] = self.source_url
        self.source_dir = os.path.join(cwd, 'build', self.application_id)
        self.metadata_file = os.path.join(cwd, 'metadata', self.application_id + '.yml')
        if 'PERSONAL_ACCESS_TOKEN' in os.environ:
            self.gitlab_api_key = os.getenv('PERSONAL_ACCESS_TOKEN')
        if 'GITHUB_TOKEN' in os.environ:
            self.github_api_key = os.getenv('GITHUB_TOKEN')
        if 'VIRUSTOTAL_API_KEY' in os.environ:
            self.virustotal_api_key = os.getenv('VIRUSTOTAL_API_KEY')

    def add_label(self, labels):
        if isinstance(labels, str):
            self.reply['labels'].add(labels)
        else:
            for label in labels:
                self.reply['labels'].add(label)

    def is_ci(self):
        """check whether running in a CI environment as root"""
        return 'CI' in os.environ and os.getuid() == 0

    def run_gradle_plugin(self, name, task=None):
        home = os.getenv('GRADLE_USER_HOME')
        if not home:
            home = os.path.join(os.getenv('HOME'), '.gradle')
        plugin = os.path.join(home, '%s.gradle' % name)
        if os.path.exists(plugin):
            cmd = [self.gradlew, '--init-script', plugin]
            if task:
                cmd.append(task)
            p = run_cli_tool(cmd, self.source_dir)
            print(p.stdout.decode())

    def get_build_gradle_with_android_plugin(self):
        """get all build.gradle(.kts) files with the com.android.application plugin"""
        paths = common.get_all_gradle_and_manifests(self.source_dir)
        self.gradle_subdir = str(common.get_gradle_subdir(self.source_dir, paths))
        g = glob.glob(
            os.path.join(self.source_dir, self.gradle_subdir, 'build.gradle*')
        )
        if not g:
            return ''
        self.add_label('gradle')
        gradlew_path = os.path.join(self.source_dir, 'gradlew')
        if os.path.exists(gradlew_path):
            os.chmod(gradlew_path, 0o700)  # sometimes devs forget...
            self.gradlew = './gradlew'
        else:
            self.gradlew = '/usr/bin/gradle'
        return g

    def get_gitlab_api(self):
        return gitlab.Gitlab(
            'https://gitlab.com', api_version=4, private_token=self.gitlab_api_key
        )

    def calc_apk_checksums(self):
        """Calculate MD5, SHA1, and SHA256 for all APKs and cache them"""
        if not hasattr(self, 'apk_checksums'):
            self.apk_checksums = dict()
            for apk in self.apkfiles:
                md5 = hashlib.md5()  # nosec used as backwards compatible ID
                sha1 = hashlib.sha1()  # nosec used as backwards compatible ID
                sha256 = hashlib.sha256()
                with open(apk, 'rb') as f:
                    while True:
                        t = f.read(16384)
                        if len(t) == 0:
                            break
                        md5.update(t)
                        sha1.update(t)
                        sha256.update(t)
                self.apk_checksums[os.path.basename(apk)] = {
                    'md5': binascii.hexlify(md5.digest()).decode(),
                    'sha1': binascii.hexlify(sha1.digest()).decode(),
                    'sha256': binascii.hexlify(sha256.digest()).decode(),
                }
            self.reply['reportData']['apkChecksums'] = self.apk_checksums

    def apkfiles_required(self):
        """Run this first for modules that require APK files to analyze"""
        if self.apkfiles:
            return True
        else:
            print('Skipping, no APK files found')
            self.write_json()
            return False

    def get_current_commit_id(self):
        if not os.path.isdir(os.path.join(self.source_dir, '.git')):
            return
        commit_id = (
            subprocess.check_output(
                ['git', '-C', self.source_dir, 'describe', '--always', '--tags']
            )
            .decode()
            .strip()
        )
        self.reply['commitId'] = commit_id
        return commit_id

    def get_current_commit_id_url(self, commit_id=None):
        if not os.path.isdir(os.path.join(self.source_dir, '.git')):
            return
        if commit_id is None:
            commit_id = self.get_current_commit_id()
        if re.compile(
            r"https://(github\.com|gitlab\.com|framagit\.org|git.sr.ht)/"
        ).match(self.source_url):
            tag_path = 'tree'
        elif re.compile(r"https://(codeberg\.org|notabug\.org|bitbucket\.org)/").match(
            self.source_url
        ):
            tag_path = 'src'
        else:
            for tag_path in ('tree', 'src'):
                tag_url = os.path.join(self.source_url, tag_path, commit_id)
                try:
                    r = requests_head(tag_url)
                    r.raise_for_status()
                    return tag_url
                except Exception as e:
                    print(e)
            else:
                return
        return os.path.join(self.source_url, tag_path, commit_id)

    def get_source_scanning_header(self, text):
        """Generate the HTML for a header of a module that scans the source code"""
        return '<h3>%s @ <a href="%s" target="_blank"><tt>%s</tt></a></h3>' % (
            text,
            self.get_current_commit_id_url(),
            self.get_current_commit_id(),
        )

    def get_source_url(self, f):
        repo = git.repo.Repo(self.source_dir)
        commit_id = binascii.hexlify(bytearray(repo.head.commit.binsha)).decode()
        self.reply['commitId'] = commit_id
        return os.path.join(self.source_url, 'blob', commit_id, f)

    def write_json(self):
        with open(
            os.path.join(get_json_dir(), os.path.basename(sys.argv[0]) + '.json'), 'w'
        ) as fp:
            json.dump(self.reply, fp, indent=2, sort_keys=True, ensure_ascii=False, cls=Encoder)

    def write_local_test_report(self):
        with open(os.path.join(self.base_dir, 'test_report.html'), 'w') as fp:
            fp.write(
                '<html><head><meta http-equiv="refresh" content="10"></head><body>'
            )
            fp.write(self.reply['report'])
            fp.write('</body></html>')


def get_job_issue_ids():
    return (
        os.getenv('CI_JOB_ID', str(int(datetime.utcnow().timestamp()))).strip(),
        os.getenv('ISSUEBOT_CURRENT_ISSUE_ID', '0').strip(),
    )


def parse_git_urls_from_description(description):
    """parse valid git URLs from a block of text

    This maintains the order in which the URLs were found to give
    priority to the URLs which the user has written first in the
    description.  Duplicates are not included.  The position in the
    list is based on when the URL was first seen.

    """

    with open('data/gitlab-reserved-names.json') as fp:
        gitlab_reserved_names = json.load(fp)
    with open('data/github-reserved-names.json') as fp:
        github_reserved_names = json.load(fp)

    git_urls = []
    for m in GIT_PATTERN.finditer(description):
        url = m.group(0)
        if url.startswith('http://'):
            url = 'https://' + url[7:]
        if url.endswith('.git'):
            url = url[:-4]
        # skip fdroid repos and GitLab/GitHub reserved group names
        first_segment = urlparse(url).path.split('/')[1]
        if (
            (
                url.startswith('https://gitlab.com/')
                and first_segment
                not in ['', 'fdroid', 'f-droid'] + gitlab_reserved_names
            )
            or (
                url.startswith('https://codeberg.org/')  # a GitLab instance
                and first_segment
                not in ['', 'fdroid', 'f-droid'] + gitlab_reserved_names
            )
            or (
                url.startswith('https://framagit.org/')  # a GitLab instance
                and first_segment
                not in ['', 'fdroid', 'f-droid'] + gitlab_reserved_names
            )
            or (
                url.startswith('https://github.com/')
                and first_segment
                not in ['', 'fdroid', 'f-droid'] + github_reserved_names
            )
        ):
            if url not in git_urls:
                git_urls.append(url)
    return git_urls


def get_json_dir():
    project_dir = os.getenv('CI_PROJECT_DIR', os.getcwd())
    job_dir, issue_id = get_job_issue_ids()
    d = os.path.join(project_dir, ISSUEBOT_API_DIR, job_dir, issue_id)
    os.makedirs(d, exist_ok=True)
    return d


def download_file(url, dldir='repo'):
    try:
        net.download_file(url, dldir=dldir)
    except requests.exceptions.HTTPError as e:
        print(e)


def read_properties(path):
    with open(path, errors="surrogateescape") as fp:
        text = fp.read()
    config = configparser.ConfigParser()
    config.read_string("[DEFAULT]\n" + text)  # fake a INI file
    return text, config["DEFAULT"]


def requests_get(url):
    return requests.get(url, headers=HEADERS, timeout=REQUESTS_TIMEOUT)


def requests_head(url):
    return requests.head(url, headers=HEADERS, timeout=REQUESTS_TIMEOUT)


def run_cli_tool(cmd, cwd=None, timeout=600):
    """Run a command line tool with output, exceptions caught and printed"""
    print(Fore.GREEN + '# ', ' '.join(cmd) + Style.RESET_ALL)
    try:
        return subprocess.run(
            cmd,
            cwd=cwd,
            stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT,
            timeout=timeout,
        )
    except (OSError, subprocess.TimeoutExpired) as e:
        print(Fore.LIGHTYELLOW_EX + str(e) + Style.RESET_ALL)
        p = subprocess.CompletedProcess(cmd, 0x8BADF00D)
        p.stdout = str(e).encode()
        return p


def get_modules():
    modules = []
    for f in glob.glob(
        os.path.join(os.path.dirname(os.path.dirname(__file__)), 'modules', '*.*')
    ):
        if (
            f[-1] == '~'
            or not os.path.isfile(f)
            or not os.access(f, os.R_OK)
            or not os.access(f, os.X_OK)
        ):
            print(
                '\n===================================================================\n',
                'Skipping',
                f,
            )
        else:
            modules.append(f)
    return sorted(modules)


def setup_for_modules():
    if 'CI' not in os.environ or os.getuid() != 0:
        print(
            Fore.YELLOW
            + 'WARNING: not running in CI environment as root!'
            + Style.RESET_ALL
        )
        return

    cmd = [
        'bash',
        'fdroidserver/buildserver/provision-apt-get-install',
        'http://deb.debian.org/debian',
    ]
    run_cli_tool(cmd)

    apt_regex = re.compile(
        r'''\s*(?:#|//|/\*)\s*issuebot_apt_install\s*[:= ]\s*([^#]+?)\s*(?:\*/)?\s*\n'''
    )
    pip_regex = re.compile(
        r'''\s*(?:#|//|/\*)\s*issuebot_pip_install\s*[:= ]\s*([^#]+?)\s*(?:\*/)?\s*\n'''
    )
    for f in get_modules():
        print(
            '\n===================================================================\n',
            'Setting up',
            f,
        )
        with open(f) as fp:
            data = fp.read()
        apt = apt_regex.search(data)
        if apt:
            cmd = ['apt-get', 'install'] + apt.group(1).strip().split()
            run_cli_tool(cmd)
        pip = pip_regex.search(data)
        if pip:
            cmd = ['pip3', '--timeout', '100', '--retries', '10', 'install']
            cmd += pip.group(1).strip().split()
            run_cli_tool(cmd)
